package ru.dreendex.translationshelper;

import ru.dreendex.translationshelper.lang.Message;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

public class LangTreeNode extends DefaultMutableTreeNode {

    private String lang;
    private String path;
    private Message message;

    public LangTreeNode(String lang) {
        this(lang, lang);
    }

    public LangTreeNode(String lang, String path) {
        this.lang = lang;
        this.path = path;
        this.setUserObject(this);
    }

    public LangTreeNode(String lang, Message message) {
        this(lang, message.getKey());
        this.message = message;
    }

    public String getLang() {
        return lang;
    }

    public Message getMessage() {
        return message;
    }

    public String getFullPath() {
        TreeNode node = this.getParent();
        if (node != null) {
            return node instanceof LangTreeNode ? ((LangTreeNode) node).getFullPath() : node.toString() + "." + this.toString();
        }
        return this.toString();
    }

    @Override
    public String toString() {
        return path;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LangTreeNode) {
            return this.path.equals(((LangTreeNode) obj).path);
        } else {
            return super.equals(obj);
        }
    }
}
